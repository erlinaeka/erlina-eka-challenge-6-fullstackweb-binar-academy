'use strict';
const bcrypt = require('bcrypt');
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [{
      email : 'erlinaeka@gmail.com',
      username : 'erlinaeka',
      password: bcrypt.hashSync("erlinaeka123", 10),
      role: 'superadmin',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', { email: 'erlinaeka@gmail.com' }, {});
  }
};
