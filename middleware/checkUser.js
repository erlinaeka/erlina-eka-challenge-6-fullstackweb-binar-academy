const userRepository = require('../repository/userRepository')
const authorization = require('../library/authorization')
const responseLibrary = require('../library/response')

async function verifSuperAdmin(req, res, next) {
    let auth    = req.headers["authorization"];
    if(auth){
        try {
            let token       = auth.slice(7);
            var tokenVerify = authorization.verifyJWT(token);
            console.log("ini token:" + token)
            console.log("ini tokenverify:" + tokenVerify)
            const getRole = await userRepository.getDataToken(tokenVerify.email)
            if(getRole.role=="superadmin"){
                next();
            }else{
                responseLibrary(res, 401, "Failed", "Unauthorized", getRole)
            }
        }catch(err){
            responseLibrary(res, 404, "Failed", "Token Failed", [])
        }
    }else{
        responseLibrary(res, 400, "Failed", "Token Required", [])
    }
};

async function verifAdmin(req, res, next) {
    let auth    = req.headers["authorization"];
    if(auth){
        try {
            let token       = auth.slice(7);
            var tokenVerify = authorization.verifyJWT(token);
            console.log("ini token:" + token)
            console.log("ini tokenverify:" + tokenVerify)
            const getRole = await userRepository.getDataToken(tokenVerify.email)
            if(getRole.role=="admin"){
                next();
            }else{
                responseLibrary(res, 401, "Failed", "Unauthorized", getRole)
            }
        }catch(err){
            responseLibrary(res, 404, "Failed", "Token Failed", [])
        }
    }else{
        responseLibrary(res, 400, "Failed", "Token Required", [])
    }
};

async function verifAdminOrSuperadmin(req, res, next) {
    let auth    = req.headers["authorization"];
    if(auth){
        try {
            let token       = auth.slice(7);
            var tokenVerify = authorization.verifyJWT(token);
            console.log("ini token:" + token)
            console.log("ini tokenverify:" + tokenVerify)
            const getRole = await userRepository.getDataToken(tokenVerify.email)
            if(getRole.role=="admin" || getRole.role=="superadmin"){
                next();
            }else{
                responseLibrary(res, 401, "Failed", "Unauthorized", getRole)
            }
        }catch(err){
            responseLibrary(res, 404, "Failed", "Token Failed", [])
        }
    }else{
        responseLibrary(res, 400, "Failed", "Token Required", [])
    }
};


async function verifToken(req, res, next){
    let auth    = req.headers["authorization"];
    if(auth){
        try {
            let token       = auth.slice(7);
            var tokenVerify = authorization.verifyJWT(token);
            console.log("ini token:" + token)
            console.log("ini tokenverify:" + tokenVerify)
            const getDataToken = await userRepository.getDataToken(tokenVerify.email)
            //check ini
            if(getDataToken!=null){
                next();
            }else{
                responseLibrary(res, 404, "Failed", "User not found", [])
            }
        }catch(err){
            responseLibrary(res, 404, "Failed", "Token Failed", [])
        }
    }else{
        responseLibrary(res, 400, "Failed", "Token Required", [])
    }
}

module.exports = {
    verifSuperAdmin,
    verifAdmin,
    verifAdminOrSuperadmin,
    verifToken
}