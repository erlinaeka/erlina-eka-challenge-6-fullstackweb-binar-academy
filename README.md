# Erlina Eka Fitriani - Challenge 6 FullstackWeb Binar Academy

### Endpoint ke Halaman Open API Car Management API

http://localhost:3000/api-docs

### Email dan Password Super Admin

Email : erlinaeka@gmail.com
Password : erlinaeka123

### Create db, migration and seeding with sequelize

```
sequelize db:create
sequelize db:migrate
npx sequelize-cli db:seed:all
```

### Run Program

```
npm install
npm run serve
```
