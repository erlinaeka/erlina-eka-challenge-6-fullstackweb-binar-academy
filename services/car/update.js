const carRepository = require('../../repository/carRepository')
const userRepository = require('../../repository/userRepository')
const responseLibrary = require('../../library/response')
const authorization = require('../../library/authorization')
var multiparty = require('multiparty');
const randomstring	= require("randomstring");
const path 			= require('path');
const fs 			= require('fs');

async function updateCar(req, res){
    // res.send("Ok!")
    // 1.Ambil username based token pada header
    let auth    = req.headers["authorization"];
    let token       = auth.slice(7);
    var tokenVerify = authorization.verifyJWT(token);
    const getUser = await userRepository.getDataToken(tokenVerify.email)
    // res.send(getUser)

    // 2.Update data memanggil carRepository
    var form = new multiparty.Form();
    form.parse(req, async function(err, fields, files) {
        console.log(files. imageCar[0])
        var ext = path.extname(files. imageCar[0].originalFilename);
        var objExt = ext.split(".")
        var filename = randomstring.generate(6);
        var readerStream = fs.createReadStream(files. imageCar[0].path);
        var dest_file = path.join(process.env. IMAGES_DIRECTORY, filename + "." + objExt[objExt.length - 1]);
        var writerStream = fs.createWriteStream(dest_file);
        readerStream.pipe(writerStream);
        // res.send(fields.availableAt[0])
        //cek data car dulu. ada nggak data based fields.id
        const getCar = await carRepository.getCarByID(parseInt(fields.id))
        if(getCar!=null){
            //kalo ada jalankan hapus img terdahulu
            var oldImage = getCar.imageCar
            if(fs.existsSync(process.env.IMAGES_DIRECTORY+"/"+oldImage)){
                fs.unlinkSync(process.env.IMAGES_DIRECTORY+"/"+oldImage);
                //setelah terhapus jalankan updateCar
                const carUpdate = await carRepository.updateCar({id:fields.id[0], nama:fields.name[0], rentPerDay:fields.rentPerDay[0], size:fields.size[0], year:fields.year[0], capacity:fields.capacity[0], imageCar:filename + "." + objExt[objExt.length - 1], availableAt:fields.availableAt[0], username:getUser.username})
                responseLibrary(res, 200, "Success", "Update Success ", carUpdate)
            }else{
                const carUpdate = await carRepository.updateCar({id:fields.id[0], nama:fields.name[0], rentPerDay:fields.rentPerDay[0], size:fields.size[0], year:fields.year[0], capacity:fields.capacity[0], imageCar:filename + "." + objExt[objExt.length - 1], availableAt:fields.availableAt[0], username:getUser.username})
                responseLibrary(res, 200, "Success", "Update Success ", carUpdate)
            }
                
        }else{
            responseLibrary(res, 204, "Success", "Data Car not exist", [])
        }
       
    }); 
}


module.exports = updateCar