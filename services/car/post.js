const carRepository = require('../../repository/carRepository')
const userRepository = require('../../repository/userRepository')
const responseLibrary = require('../../library/response')
const authorization = require('../../library/authorization')
var multiparty = require('multiparty');
const randomstring	= require("randomstring");
const path 			= require('path');
const fs 			= require('fs');

async function postCar(req, res){
    // 1.Ambil username based token pada header
    let auth    = req.headers["authorization"];
    let token       = auth.slice(7);
    var tokenVerify = authorization.verifyJWT(token);
    const getUser = await userRepository.getDataToken(tokenVerify.email)

    // 2.Tambah data memanggil carRepository
    var form = new multiparty.Form();
    form.parse(req, async function(err, fields, files) {
        console.log(files. imageCar[0])
        var ext = path.extname(files. imageCar[0].originalFilename);
        var objExt = ext.split(".")
        var filename = randomstring.generate(6);
        var readerStream = fs.createReadStream(files. imageCar[0].path);
        var dest_file = path.join(process.env. IMAGES_DIRECTORY, filename + "." + objExt[objExt.length - 1]);
        var writerStream = fs.createWriteStream(dest_file);
        readerStream.pipe(writerStream);
        // res.send(fields) 
        // res.send(getUser.username)        
        // res.send({name:fields.name[0], rentPerDay:fields.rentPerDay[0], size:fields.size[0], year:fields.year[0], capacity:fields.capacity[0], imageCar:filename + "." + objExt[objExt.length - 1], availableAt:fields.availableAt[0], username:getUser.username})
        var carCreate = await carRepository.postCar({name:fields.name[0], rentPerDay:fields.rentPerDay[0], size:fields.size[0], year:fields.year[0], capacity:fields.capacity[0], imageCar:filename + "." + objExt[objExt.length - 1], availableAt:fields.availableAt[0], username:getUser.username})
        responseLibrary(res, 200, "Success", "Create Success ", carCreate)
    }); 

    // res.send("Ok!");
    //tambah data mobil
    //tambahkan juga hasil dari username yang didapat dari userrepository
    //gausah pengkondisian karena kan sebelumnya udah pengecekan di middleware terkait ada tidaknya data user dg token tsb

}


module.exports = postCar;