const carRepository = require('../../repository/carRepository')
const userRepository = require('../../repository/userRepository')
const responseLibrary = require('../../library/response')
const authorization = require('../../library/authorization')
const fs 			= require('fs');

async function deleteCar(req, res){
    //hapus car image
    const getCar = await carRepository.getCarByID(parseInt(req.params.id))
    if(getCar!=null){
        var oldImage = getCar.imageCar
        if(oldImage!=null){
            if(fs.existsSync(process.env.IMAGES_DIRECTORY+"/"+oldImage)){
                fs.unlinkSync(process.env.IMAGES_DIRECTORY+"/"+oldImage);
            }           
        }
        //end hapus car image

        //get user name who's delete data car
        let auth    = req.headers["authorization"];
        let token       = auth.slice(7);
        var tokenVerify = authorization.verifyJWT(token);
        const getUser = await userRepository.getDataToken(tokenVerify.email)
        //end
        
        //update kolom deletedby
        const updateDeletedBy = await carRepository.updateDeletedCar({username:getUser.username, carID:parseInt(req.params.id)})
        //end update kolom deletedby

        //softdelete data car
        const deleteCar = await carRepository.deleteCar(parseInt(req.params.id))
        //end softdelete data car

        responseLibrary(res, 200, "Success","Delete Success ", deleteCar)
    }else{
        responseLibrary(res, 204, "Success", `there's no exist car data with ID ${req.params.id}`, [])
    }

}


module.exports = deleteCar