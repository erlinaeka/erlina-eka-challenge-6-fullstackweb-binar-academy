const carRepository = require('../../repository/carRepository')
const responseLibrary = require('../../library/response')

async function availableCar(req, res){
    const getCarAvailable = await carRepository.getCarAvailable(req.params.availableAt);
    // res.send(getCarAvailable[0])
    if(getCarAvailable[0]!=null){
        responseLibrary(res, 200, "Success", "Success get car", getCarAvailable);
    }else{
        responseLibrary(res, 204, "Success", "No Car Available", []);
    }

}

async function allCar(req, res){
    const getAllCar = await carRepository.getAllCar();
    if(getAllCar[0]!=null){
        responseLibrary(res, 200, "Success", "Success get car", getAllCar);
    }else{
        responseLibrary(res, 204,  "Success", "No Data Car to show", []);
    }
}

module.exports = {
    availableCar,
    allCar
}