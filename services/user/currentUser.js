const userRepository = require('../../repository/userRepository')
const authorization = require('../../library/authorization')
const responseLibrary = require('../../library/response')

// ini kan berdasarkan token yang dimiliki
// nah tokennya ini dijadikan req.body atau di authorization barrer token
async function currentUser(req, res){
    let auth    = req.headers["authorization"];
    // let auth    = req.headers["authorization"];
    // let token       = auth.slice(7);
    // var tokenVerify = authorization.verifyJWT(token);
    // res.send(tokenVerify.email)
    let token       = auth.slice(7);
    var tokenVerify = authorization.verifyJWT(token);
    const getCurrentUser = await userRepository.getDataToken(tokenVerify.email)
    responseLibrary(res, 200, "Success", "Success get Current User based on JWT Token", getCurrentUser)
}


module.exports = currentUser;