const userRepository = require('../../repository/userRepository')
const responseLibrary = require('../../library/response')

async function addAdmin(req, res){  
    const checkRegisteredUser = await userRepository.checkEmail(req.body.email)
    if(checkRegisteredUser){
        responseLibrary(res, 409, "Failed", "Register Admin Failed, Email already registered", checkRegisteredUser);
    }else{
        const addAdmin = await userRepository.addAdmin({
            email:req.body.email,
            username:req.body.username,
            password:req.body.password
        })
        responseLibrary(res, 200, "Success", "Register Admin Success", addAdmin);
    }
}
async function updateAdmin(req, res){  
    const updateAdmin = await userRepository.updateAdmin(req.body.role, req.body.id)
    if(updateAdmin[0]!=0){
        responseLibrary(res, 200, "Success", "User Role success change to Admin", updateAdmin);
    }else{
        responseLibrary(res, 204, "Success", "Update Admin Failed, check id", [])
    }
}

module.exports={
    addAdmin,
    updateAdmin
}