//1.manggil fungsi login untuk ngecek dengan email
//2.kalo emailnya gaada !email balikin respon error (401)
//3.elsenya kalo email ada dia ngecek passwordnya sama nggak, kalo iya oke kalo ngga ya else
const userRepository = require('../../repository/userRepository')
const authorization = require('../../library/authorization')
const responseLibrary = require('../../library/response')

async function login(req, res){  
    const loginUser = await userRepository.checkEmail(req.body.email)
    if(loginUser!=null){
        const checkPassword = await userRepository.checkPassword(req.body.password, loginUser.password)
        if(checkPassword==true){
            console.log(authorization.generateJWT(req.body.email))
            authorization.generateJWT(req.body.email)
            responseLibrary(res, 200, "Success", "Login Success", {"email" : loginUser.email, "Token" : authorization.generateJWT(req.body.email) })
        }else{
            responseLibrary(res, 403, "Failed", "Login Failed, Password Wrong", [])     
        }

    }else{
        responseLibrary(res, 404, "Failed", "Login Failed, Email not registered", [])
    }

}

module.exports=login;