const userRepository = require('../../repository/userRepository')
const responseLibrary = require('../../library/response')

async function register(req, res){  
    const checkRegisteredUser = await userRepository.checkEmail(req.body.email)
    if(checkRegisteredUser){
        responseLibrary(res, 409, "Failed", "Register Failed, Email already registered", checkRegisteredUser )
    }else{
        const checkRegisteredUsername = await userRepository.checkUsername(req.body.username)
        if(checkRegisteredUsername){
            responseLibrary(res, 409, "Failed", "Register Failed, Username already taken", checkRegisteredUsername )
        }else{
            const registerUser = await userRepository.registerUser({
                email:req.body.email,
                username:req.body.username,
                password:req.body.password
            })
            responseLibrary(res, 200, "Success", "Register Success", registerUser)
        }
    }
}

module.exports=register;