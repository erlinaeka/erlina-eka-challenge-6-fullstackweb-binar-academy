const moment = require('moment-timezone');

function response(res, status, message, display_message, data){
    var result = {
        status: status,
        message: message,
        display_message: display_message,
        data: data,
        time:moment.tz(new Date(), 'Asia/Jakarta').format('DD-MM-YYYY HH:mm')
    }
    res.setHeader("Content-Type", "application/json");
    res.writeHead(status);
    res.end(JSON.stringify(result, null, 2));  
}


module.exports = response