// require thirdperty module
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const registerService = require('./services/user/register')
const loginService = require('./services/user/login')
const adminService = require('./services/user/admin')
const currentUserService = require('./services/user/currentUser')
const carPostService = require('./services/car/post')
const carDeleteService = require('./services/car/delete')
const carUpdateService = require('./services/car/update')
const carGetService = require('./services/car/get')
const checkUser = require('./middleware/checkUser');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./src/Swagger Car Management API.json');

const app = express();
// parse application/json

app.use(bodyParser.json())
app.use(cors());
app.use(express.json());
// setting static file
app.use(express.static('public'))

app.get('/', (req, res)=>{
    res.send("erlina eka");
})

app.post('/user/login', loginService)
app.post('/user/register', registerService)
app.post('/user/add-admin', checkUser.verifSuperAdmin, adminService.addAdmin)
app.put('/user/update-admin', checkUser.verifSuperAdmin, adminService.updateAdmin)
app.get('/user/current-user', checkUser.verifToken, currentUserService)

app.get('/car', checkUser.verifAdminOrSuperadmin, carGetService.allCar)
app.get('/car/available/:availableAt', carGetService.availableCar)
app.post('/car', checkUser.verifAdminOrSuperadmin, carPostService)
app.put('/car', checkUser.verifAdminOrSuperadmin, carUpdateService)
app.delete('/car/:id', checkUser.verifAdminOrSuperadmin, carDeleteService)

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(process.env.PORT_API, () => {
    console.log(`Example app listening on port ${process.env.PORT_API}`)
})