const { Car }    = require('../models')

async function postCar(dataCar){
    const postCar = await Car.create({
        name: dataCar.name,
        rentPerDay: dataCar.rentPerDay,
        size: dataCar.size,
        year: dataCar.year,
        capacity: dataCar.capacity,
        imageCar: dataCar.imageCar,
        availableAt: dataCar.availableAt,
        createdBy: `Created by ${dataCar.username}`,
        updatedBy: `Updated by ${dataCar.username}`,
        deletedBy: `Data exist`
    }, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    });
    return postCar;
}

async function getCarByID(idCar){
    const getCar = await Car.findOne({ 
        where: { id: idCar}
    }).then(function (result) {
        return result
    });
    return getCar;
}

async function getAllCar(){
    const getCar = await Car.findAll().then(function (result) {
        return result;
    });
    return getCar;
}

async function getCarAvailable(availableDate){
    const getCarAvailable = await Car.findAll({
        where: {
            availableAt: availableDate
        }
    }).then(function (result) {
        return result;
    });
    return getCarAvailable;
}

async function updateCar(dataCar){
    const updatedataCar = await Car.update({
        name: dataCar.nama,
        rentPerDay: dataCar.rentPerDay,
        size: dataCar.size,
        year: dataCar.year,
        capacity: dataCar.capacity,
        imageCar: dataCar.imageCar,
        availableAt: dataCar.availableAt,
        updatedBy: `Updated by ${dataCar.username}`,
    }, {where: {id: dataCar.id}}).then(function (result) {
        return result;
    });
    return updatedataCar;
}


async function updateDeletedCar(dataCar){
    const updateDeletedCar = await Car.update({
        updatedBy: `Updated by ${dataCar.username}`,
        deletedBy: `Deleted by ${dataCar.username}`
    }, {where: {id: dataCar.carID}}).then(function (result) {
        return result;
    });
    return updateDeletedCar;
}

async function deleteCar(idCar){
    const deleteCar = await Car.destroy({ 
        where: { id: idCar}
    }).then(function (result) {
        return result
    });
    return deleteCar;
}


module.exports = {
    postCar,
    getAllCar,
    getCarAvailable,
    getCarByID,
    updateCar,
    updateDeletedCar,
    deleteCar
}