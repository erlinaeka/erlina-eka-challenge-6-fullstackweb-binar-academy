const { User }    = require('../models')
const bcrypt = require('bcrypt');
const { user } = require('pg/lib/defaults');
const req = require('express/lib/request');

async function registerUser(dataUser){
    // 1.findOne kalo nama emailnya udah ada di database kirim datanya lalu yang diservices itu messagenya email udah terdaftar
    const registerUser = await User.create({
        email: dataUser.email,
        username: dataUser.username,
        password: bcrypt.hashSync(dataUser.password, 10),
        role:"member", 
    }, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    });
    return registerUser;
}

async function checkEmail(emailUser){
    const checkEmail = await User.findOne({ 
        where: { email: emailUser} 
    }, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    })
    return checkEmail ;
}

async function checkUsername(usernameUser){
    const checkUsername = await User.findOne({ 
        where: { username: usernameUser} 
    }, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    })
    return checkUsername ;
}

function checkPassword(encriptPassword, userPassword){
    return bcrypt.compareSync(encriptPassword, userPassword)
}

async function addAdmin(dataUser){
    // 1.findOne kalo nama emailnya udah ada di database kirim datanya lalu yang diservices itu messagenya email udah terdaftar
    const addAdmin = await User.create({
        email: dataUser.email,
        username: dataUser.username,
        password: bcrypt.hashSync(dataUser.password, 10),
        role:"admin", 
    }, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    });
    return addAdmin;
}
async function updateAdmin(roleUser, idUser){
    const updateAdmin = await User.update({
        role: roleUser
    }, {where: {id: idUser}}, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    });
    return updateAdmin;
}

async function getDataToken(emailUser){
    const getDataToken= await User.findOne({
        where: { email: emailUser} 
    }, (err, result) => {
        if(err){
            return err;
        }else{
            return result;
        }
    });
    return getDataToken;
}



module.exports = {
    registerUser,
    checkEmail,
    checkUsername,
    checkPassword,
    addAdmin,
    updateAdmin,
    getDataToken
};
