'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Car.init({
    name: DataTypes.STRING,
    rentPerDay: DataTypes.INTEGER,
    size: DataTypes.STRING,
    year: DataTypes.STRING,
    capacity: DataTypes.INTEGER,
    imageCar: DataTypes.STRING,
    availableAt: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    deletedBy: DataTypes.STRING
  }, {
    sequelize,
    paranoid:true,
    deletedAt:'deletedAt',
    modelName: 'Car',
  });
  return Car;
};